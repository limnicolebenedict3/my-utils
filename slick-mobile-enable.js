var slider = jQuery('.hp-bldg .bldg-list');

// enable slick on mobile view

jQuery(window).on('resize load', function() {

    var isMobile = jQuery(window).width() > 991 ? false : true;
    
    if(isMobile) {

        if(!slider.hasClass('slick-initialized')) {
            slider.slick({
                dots: false,
                infinite: false,
                speed: 1000,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: false,
                responsive: [
                    {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false
                    }
                    },
                    {
                    breakpoint: 481,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false
                    }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        }

    } else {

        if(slider.hasClass('slick-initialized')) {
            slider.slick('unslick');
        }

    } 
});
