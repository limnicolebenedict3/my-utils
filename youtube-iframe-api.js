/**
 *  sample structure
 *                     
    <div class="sr-vid-tv yt-video-item">
        <div class="sr-vid-tv-video">

            <!-- video placeholder -->
            <canvas style="background-image: url('/wp-content/themes/scarlessnose.com/images/closed-scarless-rhinoplasty/vid-placeholder.jpg');"></canvas>
            <!-- end video placeholder -->

            <!-- video srource -->
            <iframe class="yt-embed-video" width="560" height="315" src="https://www.youtube.com/embed/ZkdTc-v4hIw?controls=0&version=3&enablejsapi=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            <!-- end video srource -->
            
        </div>
        <div class="sr-vid-play-btn">
            <span class="ai-font-play-button-a sr-vid-play-icon"></span>
            <span class="ai-font-pause-button-a sr-vid-pause-icon"></span>
        </div>
    </div>
 * 
 * 
 * **/ 
					


// initialize youtube api script
function appendYTApiScript() {
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}


function initializePlayer(players) {
    var playersArray = []; // iframe player collection
    var playersElement = jQuery(players);
    
    playersElement.each(function(key, value) {
        var player;
        var iframe = jQuery(value).find('iframe');
        
        // adds id to the current iframe
        iframe.attr('id', 'yt-iframe-' + key);
        

        player = new YT.Player(iframe[0] , {
            events: {
            'onReady': initializeIframe,
            'onStateChange': iframeStateChange
            }
        });

        playersArray.push(player);

    });


    function initializeIframe(event) {
        var iframe = jQuery(event.target.f);
        var iframeId = iframe.attr('id').split('-')[2];
        var player  = playersArray[iframeId];
        var videoItem = iframe.parents('.yt-video-item');
        
    
        videoItem.click(function() {
           

            if(videoItem.hasClass('is-playing')) {
                player.pauseVideo();
                videoItem.removeClass('is-playing');
                return;
            }

            player.playVideo();

            videoItem.addClass('is-playing');

        });
    
    }

    function iframeStateChange(event) {
        var state = event.data;
        var iframe = jQuery(event.target.f);
        var iframeId = iframe.attr('id').split('-')[2];
        var player  = playersArray[iframeId];
        var videoItem = iframe.parents('.yt-video-item');

        // go to tho original state when the player ends
        if(state === 0) {

            player.seekTo(0, false);
            
            videoItem.removeClass('is-playing');

        }
    }

    
}


appendYTApiScript();

// Initialize Players when the youtube iframe api script has been loaded
window.onYouTubeIframeAPIReady = function() {
        
    initializePlayer('.yt-video-item');

    console.log('youtube iframe api initialized');
}
