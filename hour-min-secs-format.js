// get hours
const timeStamp = 2000;
const hours     = Math.floor(timeStamp / 60 / 60);
const minutes   = Math.floor((timeStamp / 60) - (hours * 60));
const seconds   = timeStamp % 60;
