
jQuery(window).on('scroll orientationchange load',function() {

   if (jQuery(window).width() > 991) {

      if( jQuery(this).scrollTop()  > 150 && jQuery(window).width() > 991 ){
         jQuery('header.hdr-main').addClass('scrolled');
      } else {
         jQuery('header.hdr-main').removeClass('scrolled');
      }
      
   }

});

jQuery(window).on("resize", function(){
    if( jQuery(window).width() < 992){
        jQuery('header.main-header').removeClass('scrolled animated fadeInDown fadeIn');
    }
});

jQuery(document).on("load resize scroll orientationchange", function(){
    var scrollVar = window.pageYOffset || document.documentElement.scrollTop;
    if
        ( jQuery(window).width() > 991 && scrollVar > 230){
        jQuery('header.main-header').addClass('scrolled animated fadeInDown');
        jQuery('header.main-header').removeClass('fadeIn');
    }
    else
    {
        jQuery('header.main-header').removeClass('scrolled animated fadeInDown');
    }
});

