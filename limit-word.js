function limitWord(string, maxWord) {
    var wordLength = string.split(' ').length;
    var wordArr    = string.split(' ');
    var newString  = '';
    var maxWord = maxWord > wordLength ? wordLength : maxWord;
  
    if(maxWord < 1) {
      maxWord = 1;
    }
  
    if(!wordLength > maxWord) {
       return string;
    }
    
    for (let x = 0; x < maxWord; x++) {
      newString +=  ' ' + wordArr[x];
    }
    
    return newString + '...';
  }
  