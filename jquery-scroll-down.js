jQuery('.scroll-btn').on('click', function() {
	var target       = jQuery(this).attr('data-target'); // get id of the target section
	var headerHeight = jQuery('.main-header').outerHeight();
	var mobileHeader = jQuery('.amh-header-buttons').outerHeight();
	var offset       = jQuery(window).width() <= 991 ? mobileHeader : headerHeight;

	jQuery('html, body').animate(
		{
			scrollTop: (jQuery(target).offset().top - offset)
		},
		1000
	);
});
